<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TestUnit extends CI_Controller
{

    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
        
    }
    

    public function index()
    {
        $this->load->view('welcome_message');
        echo "Muhammad Rizki Assiddiki";
    }

    function cekNama(){
        $nama = "MUHAMMAD RIZKI";
        $harusnya = "MUHAMMAD RIZKI ASSIDDIKI";
        $testname = "TESTING CODEINGNITER GITLAB CI - DTS DEVOPS";

        echo $this->unit->run($nama, $harusnya, $testname);
    }
}
