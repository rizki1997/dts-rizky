<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Halaman extends CI_Controller {

  public function coba($pages='test')
  {

  if (!file_exists(APPPATH."views/ujicoba/".$pages.'.php')) {

      show_404();
    }

    $data['judul'] = $pages;

    $this->load->view('templates/header');
    $this->load->view('ujicoba/'.$pages, $data);
    $this->load->view('templates/footer');
  }

}
